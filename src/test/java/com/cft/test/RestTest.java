package com.cft.test;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

/**
 * @author deathNC
 */
public class RestTest {

    private List<Long> getIdListFromDictionary(JSONArray dict, String field) throws Exception {
        List<Long> result = new ArrayList<>();
        for (Object d : dict) {
            try {
                String value;
                value = (field != null) ? ((JSONObject) d).get(field).toString() : d.toString();
                result.add(Long.parseLong(value));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private void assertGETException(String url, String message) {
        boolean isExceptionThrown = false;
        try {
            JsonClient.getStringResponse(url);
        } catch (Exception e) {
            isExceptionThrown = true;
        }
        Assert.assertTrue(message, isExceptionThrown);
    }

    @Test
    public void test01_dictionary_service() throws Exception {
        JSONArray services = JsonClient.getArray("/dictionary/services");
        Assert.assertTrue("Справочник Услуги пуст!", services.size() > 0);
    }

    @Test
    public void test02_dictionary_country() throws Exception {
        List<Long> serviceIdList = new ArrayList<>();
        serviceIdList = getIdListFromDictionary(JsonClient.getArray("/dictionary/services"), "id");
        JSONArray countries = JsonClient.getArray("/dictionary/countries");
        Assert.assertTrue("Справочник Страна пуст!", countries.size() > 0);
        countries = JsonClient.getArray("/dictionary/countries-by-service?serviceId=-1");
        Assert.assertTrue("Выданы страны по несуществующим услугам: " + countries.toJSONString(), countries.isEmpty());

        int counter = 0;
        for (Long serviceId : serviceIdList) {
            countries = JsonClient.getArray("/dictionary/countries-by-service?serviceId=" + serviceId);
            counter += countries.size();
        }
        Assert.assertTrue("По всем услугам суммарно количество стран равно нулю", counter > 0);

        assertGETException("/dictionary/countries-by-service", "Обязательный параметр id услуги не был провалидирован сервером");

    }

    @Test
    public void test03_dictionary_city() throws Exception {
        Set<Long> uniqCountryId = new HashSet<>();
        List<Long> serviceIdList = getIdListFromDictionary(JsonClient.getArray("/dictionary/services"), "id");
        String msg = "Обязательные параметры не проверяются сервером: ";
        assertGETException("/dictionary/cities", msg + "serviceId и countryId");
        for (Long serviceId : serviceIdList) {
            assertGETException("/dictionary/cities?serviceId=" + serviceId, msg + "countryId");
            List<Long> countryIdList = getIdListFromDictionary(JsonClient.getArray("/dictionary/countries-by-service?serviceId=" + serviceId), null);
            for (Long countryId : countryIdList) {
                uniqCountryId.add(countryId);
                JSONArray cityList = JsonClient.getArray("/dictionary/cities?countryId=" + countryId + "&serviceId=" + serviceId);
                Assert.assertFalse("Список городов не может быть пустым для услуги #" + serviceId + "и страны #"
                        + countryId, cityList.isEmpty());
                for (Object aCityList : cityList) {
                    Long id = Long.parseLong(((JSONObject) aCityList).get("countryId").toString());
                    Assert.assertTrue("Страна в запросе справочника городов не может отличаться от страны в результирующих городах запроса",
                            countryId.equals(id));
                }

            }
        }
        for (Long countryId : uniqCountryId) {
            assertGETException("/dictionary/cities?countryId=" + countryId, msg + "serviceId");
        }
    }

    @Test
    public void test04_dictionary_trade_point() throws Exception {
        Long serviceId = getIdListFromDictionary(JsonClient.getArray("/dictionary/services"), "id").get(0);
        Long countryId = getIdListFromDictionary(JsonClient.getArray("/dictionary/countries-by-service?serviceId=" + serviceId), null).get(0);
        JSONArray cityArr = JsonClient.getArray("/dictionary/cities?countryId=" + countryId + "&serviceId=" + serviceId);
        Long cityId = getIdListFromDictionary(cityArr, "id").get(new Random().nextInt(cityArr.size()));

        String msg = "Обязательные параметры не проверяются сервером";
        assertGETException("/dictionary/trade-point", msg);
        assertGETException("/dictionary/trade-point?serviceId" + serviceId, msg);
        assertGETException("/dictionary/trade-point?countryId" + countryId, msg);
        assertGETException("/dictionary/trade-point?cityId" + cityId, msg);
        assertGETException("/dictionary/trade-point?serviceId" + serviceId + "&countryId=" + countryId, msg);
        assertGETException("/dictionary/trade-point?serviceId" + serviceId + "&cityId=" + cityId, msg);
        assertGETException("/dictionary/trade-point?countryId" + serviceId + "&cityId=" + cityId, msg);

        String url = "/dictionary/trade-point?countryId=" + countryId + "&cityId=" + cityId + "&serviceId=" + serviceId;
        JSONObject obj = JsonClient.getObject(url);
        JSONArray tradePointArr = (JSONArray)obj.get("tradePointList");
        Assert.assertFalse("Массив результатов не может быть пустым!", tradePointArr.isEmpty());
        String msg1 = "Идентификаторы услуг в параметрах запроса и результата запроса не могут отличаться: ";
        for (Object o : tradePointArr) {
            JSONObject item = (JSONObject)o;
            Assert.assertEquals(msg1 + "serviceId", serviceId, item.get("serviceId"));
            Assert.assertEquals(msg1 + "cityId", cityId, item.get("cityId"));
            Assert.assertEquals(msg1 + "countryId", countryId, item.get("countryId"));
        }
    }

}
