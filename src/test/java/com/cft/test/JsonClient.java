package com.cft.test;

import org.jboss.resteasy.client.ClientRequest;
import org.jboss.resteasy.client.ClientResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 * @author deathNC
 */
public class JsonClient {

    private static String getApiUrl() {
        return "http://localhost:8080";
    }

    public static String getStringResponse(String url) throws Exception {
        ClientRequest clientRequest = new ClientRequest(getApiUrl() + url);
        ClientResponse<String> clientResponse = clientRequest.get(String.class);
        if (clientResponse.getStatus() != 200) throw new IllegalStateException("Response status: " + clientResponse.getStatus());
        return clientResponse.getEntity();
    }

    public static JSONObject getObject(String url) throws Exception {
        JSONParser parser = new JSONParser();
        return (JSONObject)parser.parse(getStringResponse(url));
    }

    public static JSONArray getArray(String url) throws Exception {
        JSONParser parser = new JSONParser();
        return (JSONArray)parser.parse(getStringResponse(url));
    }

}
