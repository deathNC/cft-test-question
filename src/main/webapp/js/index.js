// полифилл для браузеров, в которых его нет:
if (![].includes) {
  Array.prototype.includes = function(searchElement/*, fromIndex*/) {
    'use strict';
    var O = Object(this);
    var len = parseInt(O.length) || 0;
    if (len === 0) {
      return false;
    }
    var n = parseInt(arguments[1]) || 0;
    var k;
    if (n >= 0) {
      k = n;
    } else {
      k = len + n;
      if (k < 0) {
        k = 0;
      }
    }
    while (k < len) {
      var currentElement = O[k];
      if (searchElement === currentElement ||
         (searchElement !== searchElement && currentElement !== currentElement)
      ) {
        return true;
      }
      k++;
    }
    return false;
  };
}



var dataSource = {
	country: [],
	city: [],
	service: [],
	tradePoints: [],

	availableCountries: [] // список id
};

function getDictValue(entityId, instanceId) {
	var dict = dataSource[entityId] || [];
	for (var i = 0; i < dict.length; i++) {
		if (dict[i].id == instanceId) {
			return dict[i].value;
		}
	}
	return '';
}

function getSelectedServiceId() {
	return $('#service option:selected').first()[0].value;
}

function getSelectedCountryId() {
	var value = ($('#country').val() || '').toLowerCase();
	// костыльный поиск
	for (var i = 0; i < dataSource.country.length; i++) {
		if (dataSource.country[i].value.toLowerCase() === value && dataSource.availableCountries.includes(i)) {
			return dataSource.country[i].id;
		}
	}
	return null;
}

function getSelectedCityId() {
	var value = ($('#city').val() || '').toLowerCase();
	// костыльный поиск
	for (var i = 0; i < dataSource.city.length; i++) {
		if (dataSource.city[i].value.toLowerCase() === value) {
			return dataSource.city[i].id;
		}
	}
	return null;
}

function updateCityDict() {
	var serviceId = getSelectedServiceId(),
		countryId = getSelectedCountryId();
	if (serviceId == null || countryId == null) {
		dataSource.city = [];
		return;
	}
	$.ajax({
		url: '/dictionary/cities?countryId=' + countryId + '&serviceId=' + serviceId,
		type: 'GET',
		success: function(data) {
			dataSource.city = data || [];
		},
		error: function (e) {
			console.error(e);
			alert('Произошла ошибка при загрузке справочника!');
		},
		complete: function() {
			var html = '', data = dataSource.city;
			for (var i = 0; i < data.length; i++) {
				html += '<option id="' + data[i].id + '">' + data[i].value + '</option>';
			}
			$('#cityList').html(html);
			$("#city").val('');
		}
	});
}


// init
$(document).ready(function() {
	var loadStaticDictionary = function(serviceName, localName, dataListId, callback) {
		$.ajax({
			url: '/dictionary/' + serviceName,
			type: 'GET',
			success: function(data) {
				dataSource[localName] = data || [];
				// var html = '';
				// for (var i = 0; i < dataSource[localName].length; i++) {
				// 	html += '<option id="' + localName + '-' + dataSource[localName][i].id + '">' + dataSource[localName][i].value + '</option>';
				// }
				// $('#' + dataListId).html(html);
				if (callback) callback();
			},
			error: function (e) {
				console.error(e);
				alert('Произошла ошибка при загрузке справочника!');
			}
		});
	};
	loadStaticDictionary('countries', 'country', 'countryList');
	loadStaticDictionary('services', 'service', 'serviceList', function () {
		var selectService = $('#service'), html = '<option selected></option>';
		for (var i = 0; i < dataSource.service.length; i++) {
			html += '<option value="' + dataSource.service[i].id + '">' + dataSource.service[i].value + '</option>'
		}
		selectService.html(html);
		selectService.change(function(e) {
			var id = getSelectedServiceId();
			if (isNaN(parseInt(id))) id = '';
			$.ajax({
				url: '/dictionary/countries-by-service?serviceId=' + id,
				type: 'GET',
				success: function(countryIdList) {
					dataSource.availableCountries = countryIdList;
				},
				error: function (e) {
					console.error(e);
					availableCountries = []
				},
				complete: function() {
					var html = '', idList = dataSource.availableCountries;
					for (var i = 0; i < idList.length; i++) {
						html += '<option id="' + idList[i] + '">' + getDictValue('country', idList[i]) + '</option>';
					}
					$('#countryList').html(html);
					$('#country').val('');
					updateCityDict();
				}
			});
		})
	});

	$('#buttonSearch').click(updateDataSource);

	$('#country').focusout(function() {
		updateCityDict();
	});

	// updateDataSource();
});


function updateDataSource() {
	var countryId = getSelectedCountryId(),
		cityId = getSelectedCityId(),
		serviceId = getSelectedServiceId();

	if (countryId == null || cityId == null || serviceId == null) {
		alert('Фильтры введены неверно');
		return;
	}
	
	var url = '/dictionary/trade-point?countryId=' + countryId + '&cityId=' + cityId + '&serviceId=' + serviceId + '&pageIndex=0';
	$.ajax({
		url: url,
		type: 'GET',
		success: function(data) {
			dataSource.tradePoints = data || [];
			console.dir(dataSource);
			renderTable();
		},
		error: function (e) {
			console.error(e);
		}
	});
}

function renderTable() {
	var html = '';
	for (var i = 0; i < dataSource.tradePoints.tradePointList.length; i++) {
		var item = dataSource.tradePoints.tradePointList[i];
		html += 
		'<tr>'
			+ '<td>' + item.id + '</td>'
			+ '<td>' + getDictValue('country', item.countryId) + '</td>'
			+ '<td>' + getDictValue('city', item.cityId) + '</td>'
			+ '<td>' + getDictValue('service', item.serviceId) + '</td>'
			+ '<td>' + item.value + '</td>'
			+ '<td>' + item.address + '</td>'
		+ '</tr>';
	}
	$('#tradePointRows').html(html);
}