
--create sequence hibernate_sequence start with 10000;

create table country (
  id bigint identity primary key,
  iso_code varchar(16),
  value varchar(96) not null
);

create table city (
  id bigint identity primary key,
  id_country bigint not null,
  iso_code varchar(16),
  value varchar(96) not null
);

create table service (
  id bigint identity primary key,
  value varchar(256) not null
);

create table trade_point (
  id bigint identity primary key,
  value varchar(96) not null, -- Название
  address varchar(100) not null, -- Адрес
  id_country bigint not null,
  id_city bigint not null,
  id_service bigint not null,
  constraint country_fk foreign key (id_country) references country(id),
  constraint city_fk foreign key (id_city) references city(id),
  constraint service_fk foreign key (id_service) references service(id)
);
//
create index trade_point_country_index on trade_point (id_country);
create index trade_point_service_index on trade_point (id_service);
create index trade_point_city_index on trade_point (id_city);

//
create procedure init_cities()
  modifies sql data
begin atomic
  for (select iso_code as code, id as cid from country where id is not null group by iso_code, id) do
    update city set city.id_country = cid where city.iso_code = code;
  end for;
end;
//

//
create procedure init_trade_points()
  modifies sql data
  begin atomic
    declare counter bigint;
    set counter = 0;

    for (select id_country as country_id, id as city_id from city) do
      for (select id as serviceId from service order by rand() limit 10) do
        set counter = counter + 1;
        insert into trade_point (value, address, id_country, id_city, id_service) values
          ('Точка ' || counter || ' (1)', 'Адрес ' || counter || ' (1)', country_id, city_id, serviceId),
          ('Точка ' || counter || ' (2)', 'Адрес ' || counter || ' (2)', country_id, city_id, serviceId),
          ('Точка ' || counter || ' (3)', 'Адрес ' || counter || ' (3)', country_id, city_id, serviceId),
          ('Точка ' || counter || ' (4)', 'Адрес ' || counter || ' (4)', country_id, city_id, serviceId),
          ('Точка ' || counter || ' (5)', 'Адрес ' || counter || ' (5)', country_id, city_id, serviceId),
          ('Точка ' || counter || ' (6)', 'Адрес ' || counter || ' (6)', country_id, city_id, serviceId),
          ('Точка ' || counter || ' (7)', 'Адрес ' || counter || ' (7)', country_id, city_id, serviceId),
          ('Точка ' || counter || ' (8)', 'Адрес ' || counter || ' (8)', country_id, city_id, serviceId),
          ('Точка ' || counter || ' (9)', 'Адрес ' || counter || ' (9)', country_id, city_id, serviceId),
          ('Точка ' || counter || ' (10)', 'Адрес ' || counter || ' (10)', country_id, city_id, serviceId);
      end for;
    end for;
  end;
//