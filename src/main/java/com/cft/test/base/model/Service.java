package com.cft.test.base.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Справочник: Услуга
 * @author deathNC
 */
@Entity
@Table(name = "service")
public class Service {

    @Id
    @Column(name = "id")
    @Getter @Setter private Long id;

    @Column(name = "value")
    @Getter @Setter private String value;

}
