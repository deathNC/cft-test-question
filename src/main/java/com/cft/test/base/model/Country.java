package com.cft.test.base.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Справочник: Страна
 * @author deathNC
 */
@Entity
@Table(name = "country")
public class Country {

    @Id
    @Column(name = "id")
    @Getter @Setter private Long id;

    @Column(name = "value")
    @Getter @Setter private String value;

}
