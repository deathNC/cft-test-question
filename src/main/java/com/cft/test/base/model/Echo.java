package com.cft.test.base.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author deathNC
 */
@NoArgsConstructor
@AllArgsConstructor
public class Echo {
    @Getter @Setter private String message;
}
