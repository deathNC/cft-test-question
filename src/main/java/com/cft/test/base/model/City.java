package com.cft.test.base.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Справочник: Город
 * @author deathNC
 */
@Entity
@Table(name = "city")
public class City {

    @Id
    @Column(name = "id")
    @Getter @Setter private Long id;

    @Column(name = "id_country")
    @Getter @Setter private Long countryId;

    @Column(name = "value")
    @Getter @Setter private String value;

}
