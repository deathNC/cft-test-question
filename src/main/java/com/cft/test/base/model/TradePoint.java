package com.cft.test.base.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Сущность: Услуга
 * @author deathNC
 */
@Entity
@Table(name = "trade_point")
public class TradePoint {

    @Id
    @Column(name = "id")
    @Getter @Setter private Long id;

    @ManyToOne
    @JoinColumn(name = "id_country", referencedColumnName = "id")
    @Getter @Setter private Country country;

    @ManyToOne
    @JoinColumn(name= "id_city", referencedColumnName = "id")
    @Getter @Setter private City city;

    @ManyToOne
    @JoinColumn(name = "id_service", referencedColumnName = "id")
    @Getter @Setter private Service service;

    @Column(name = "value")
    @Getter @Setter private String value;

    @Column(name = "address")
    @Getter @Setter private String address;

}
