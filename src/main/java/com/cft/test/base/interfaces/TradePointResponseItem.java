package com.cft.test.base.interfaces;

import com.cft.test.base.model.TradePoint;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author deathNC
 */
@NoArgsConstructor
public class TradePointResponseItem {
    @Getter @Setter private Long id;
    @Getter @Setter private Long countryId;
    @Getter @Setter private Long cityId;
    @Getter @Setter private Long serviceId;
    @Getter @Setter private String value;
    @Getter @Setter private String address;

    public TradePointResponseItem(TradePoint source) {
        this.id = source.getId();
        this.countryId = source.getCountry().getId();
        this.cityId = source.getCity().getId();
        this.serviceId = source.getService().getId();
        this.value = source.getValue();
        this.address = source.getAddress();
    }
}
