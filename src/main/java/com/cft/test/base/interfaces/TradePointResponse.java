package com.cft.test.base.interfaces;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author deathNC
 */
public class TradePointResponse {
    @Getter @Setter private List<TradePointResponseItem> tradePointList;
    @Getter @Setter private Integer count;
}
