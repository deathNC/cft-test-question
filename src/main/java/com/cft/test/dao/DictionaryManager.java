package com.cft.test.dao;

import com.cft.test.base.model.City;
import com.cft.test.base.model.Country;
import com.cft.test.base.model.Service;
import com.cft.test.base.model.TradePoint;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Доступ к справочникам
 * @author deathNC
 */
@Repository
public class DictionaryManager {

    public static final Integer PAGE_SIZE = 35;

    @PersistenceContext
    private EntityManager em;

    /**
     * Справочник стран
     */
    public List<Country> getCountryList() {
        return em.createQuery("select c from Country c", Country.class).getResultList();
    }

    public List<Long> getCountryList(Long serviceId) {
        String query = "select tp.country.id from TradePoint tp where tp.service.id = :serviceId group by tp.country.id";
        return serviceId == null
                ? Collections.<Long>emptyList()
                : em.createQuery(query, Long.class).setParameter("serviceId", serviceId).getResultList();
    }

    /**
     * Справочник услуг
     */
    public List<Service> getServiceList() {
        return em.createQuery("select c from Service c", Service.class).getResultList();
    }

    /**
     * Поиск города в зависимости от страны и части имени
     * @param serviceId идентификатор услуги
     * @param countryId идентификатор страны
     * @return список найденных городов
     */
    public List<City> getCityListByCountry(Long serviceId, Long countryId) {
        String query = "select c from City c where c.id in " +
                "(select tp.city.id from TradePoint tp where tp.service.id = :serviceId and tp.country.id = :countryId group by tp.city.id)";
        return serviceId == null || countryId == null ? Collections.<City>emptyList() : em.createQuery(query, City.class)
                .setParameter("serviceId", serviceId)
                .setParameter("countryId", countryId)
                .getResultList();
//        return em.createQuery("select c from City c where c.countryId = :countryId and lower(c.value) like :namePart", City.class)
//                .setParameter("countryId", countryId)
//                .setParameter("namePart", "%" + (namePart == null ? "" : namePart.toLowerCase()) + "%")
//                .setMaxResults(10)
//                .getResultList();
    }

    /**
     * Поиск торговых точек по любому сочетанию параметров
     * @param countryId идентификатор страны
     * @param cityId идентификатор города
     * @param serviceId идентификатор услуги
     * @return список найденных точек
     */
    @Deprecated // потому что параметры обязательные теперь
    public List<TradePoint> searchTradePoints(Long countryId, Long cityId, Long serviceId, Integer pageIndex) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<TradePoint> criteria = builder.createQuery(TradePoint.class);
        Root<TradePoint> tradePointRoot = criteria.from(TradePoint.class);
        criteria.select(tradePointRoot);
        List<Predicate> predicates = new ArrayList<>();
        if (serviceId != null) {
            predicates.add(builder.equal( tradePointRoot.get("service").get("id"), serviceId ) );
        }
        if (countryId != null) {
            predicates.add(builder.equal( tradePointRoot.get("country").get("id"), countryId ) );
        }
        if (cityId != null) {
            predicates.add(builder.equal( tradePointRoot.get("city").get("id"), cityId ) );
        }
        if (!predicates.isEmpty()) criteria.where(predicates.toArray(new Predicate[]{}));
        Integer offset = pageIndex * PAGE_SIZE;
        return em.createQuery(criteria).setMaxResults(PAGE_SIZE).setFirstResult(offset).getResultList();
    }

    public Long tradePointPageCount(Long countryId, Long cityId, Long serviceId) {
        String query = "select count(tp) from TradePoint tp where tp.service.id = :serviceId" +
                " and tp.country.id = :countryId and tp.city.id = :cityId";
        Long count = em.createQuery(query, Long.class)
                .setParameter("serviceId", serviceId)
                .setParameter("countryId", countryId)
                .setParameter("cityId", cityId)
                .getSingleResult();
        return count / PAGE_SIZE;
    }

}
