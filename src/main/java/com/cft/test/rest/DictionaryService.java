package com.cft.test.rest;

import com.cft.test.base.interfaces.TradePointResponse;
import com.cft.test.base.interfaces.TradePointResponseItem;
import com.cft.test.base.model.City;
import com.cft.test.base.model.Country;
import com.cft.test.base.model.Service;
import com.cft.test.base.model.TradePoint;
import com.cft.test.dao.DictionaryManager;
import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * Справочные rest-сервисы
 * @author deathNC
 */
@Log4j
@Controller
@RequestMapping(value = "/dictionary")
public class DictionaryService {

    @Autowired
    private DictionaryManager dictionaryManager;

    @RequestMapping(value = "/countries", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseBody
    public List<Country> getCountryList() {
        try {
            return dictionaryManager.getCountryList();
        } catch (Exception e) {
            log.error("Cannot select countries", e);
            throw e;
        }
    }

    @RequestMapping(value = "/countries-by-service", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseBody
    public List<Long> getCountriesByService(@RequestParam Long serviceId) {
        try {
            return dictionaryManager.getCountryList(serviceId);
        } catch (Exception e) {
            log.error("Cannot select countries", e);
            throw e;
        }
    }

    @RequestMapping(value = "/services", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseBody
    public List<Service> getServiceList() {
        try {
            return dictionaryManager.getServiceList();
        } catch (Exception e) {
            log.error("Cannot select services", e);
            throw e;
        }
    }

    @RequestMapping(value = "/cities", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseBody
    public List<City> getCountryList(@RequestParam Long serviceId, @RequestParam Long countryId) {
        try {
            return dictionaryManager.getCityListByCountry(serviceId, countryId);
        } catch (Exception e) {
            log.error("Cannot load cities", e);
            throw e;
        }
    }

    @RequestMapping(value = "/trade-point", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseBody
    public TradePointResponse getCountryList(
            @RequestParam Long countryId,
            @RequestParam Long cityId,
            @RequestParam Long serviceId,
            @RequestParam(required = false) Integer pageIndex
    ) {
        TradePointResponse response = new TradePointResponse();
        try {
            if (pageIndex == null) pageIndex = 0;
            response.setCount(dictionaryManager.tradePointPageCount(countryId, cityId, serviceId).intValue());
            List<TradePoint> tradePoints = dictionaryManager.searchTradePoints(countryId, cityId, serviceId, pageIndex);
            List<TradePointResponseItem> destItems = new ArrayList<>();
            for (TradePoint item : tradePoints) {
                destItems.add(new TradePointResponseItem(item));
            }
            response.setTradePointList(destItems);
            return response;
        } catch (Exception e) {
            log.error("Cannot load trade points", e);
            throw e;
        }
    }

}
