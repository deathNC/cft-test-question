package com.cft.test.rest;

import com.cft.test.base.model.Echo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author deathNC
 */
@Controller
@RequestMapping(value = "/echo")
public class EchoService {

    @RequestMapping(value = "/test1", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseBody
    public Echo test1(@RequestParam(required = false) String value) {
        return new Echo("You entered: " + value);
    }

    @RequestMapping(value = "/test2", method = RequestMethod.GET, produces = "application/json;charset=utf-8")
    @ResponseBody
    public Echo test2() {
        return new Echo("Привет Мир!");
    }

}
